---
title: Bardzo prosty poradnik git
---

## Inicjacja repozytorium
Utworzenie lokalnego repozytorium
```
mkdir MyProject
cd MyProject

git init
```
Utworzyć repozytorium w serwisie typu codeberg, by dodac do niego (zdalnego) utworzone lokalne repozytorium
```
git remote add origin <adres repozytorium>
git push -u origin main 
```

.gitignore wpis wyłącza/ignoruje katalog lub plik z systemu kontroli wersji, musi znajdować się w katalogu głównym repozytorium np.
```
cat .gitignore
.idea
```

## Klonowanie
```
git clone <adres repozytorium>
```
Można też sklonować swoje utworzone repozytorium z github, codeberg ominąć krok ```git init```

## Commit i Push 

Dodawania pliku/ów do złożenia:
```
git add plik.txt
git add -A #dodanie wszystkich
```
```
git status #pokazuje zmiany do złożenia
Na gałęzi testing
Twoja gałąź jest na bieżąco z „origin/testing”.

nic do złożenia, drzewo robocze czyste
```


Utworzenie commita
```
git commit -m "nazwa zmiany"
```

Wypchnięcie zmian:
```
git push #wypchniecie do repozytorium zdalnego
git --all #wypchnięcie do wszystkich
git push --mirror #wypchniecie do mirrora
```

### Usuwanie rm

```
git rm plik #usunięcie pliku z repozytorium
git rm --cached plik #usunięcie pliku z systemu kontroli, pozostawienie w katalogu
git rm -r --cached katalog
```

## Praca z gałęziami

### Nowa gałąź

```
git branch nazwa_nowej_gałęzi
```

### Sprawdzenie i przełączanie

Informacja jakie gałęzie są w reozytorium (* aktualna włączona gałąź)

```
$ git branch
  git.disroot
  * git.disroot-testing
  main
  testing
```

Sprawdzenie/przełączeni gałęzi i co jest zmodyfikowane

```
git checkout testing
Przełączono na gałąź „testing”
Twoja gałąź jest na bieżąco z „origin/testing”
```

Polecenie 'ls' pokazuje pliki w aktualnej gałęzi

### Przenoszenie plików między gałęziami
W gałęzi do której chce przenieść np. z testing do main:

Najpierw muszę być w gałęzi testing
```
git branch
  main
* testing
```
```
git checkout main -- plik.txt
git add plik.txt
git commit -m "przeniesienie z main"
git push
```




## Forki i pullrequesty

### Aktualizacja sforkowanego repozytorium

Po sklonowaniu sforkowanego repozytorium na dysk:

```
git add remote upstream <git adres repozytorium orginalnego> #gdzie upstream dowolna nazwa
```
można sprawdzić
```
git remote -v
origin  git@codeberg.org:xmszkn/gnome-sport.git (fetch)
origin  git@codeberg.org:xmszkn/gnome-sport.git (push)
upstream        git@codeberg.org:FreeSoftwarePoland/gnome-sport.git (fetch)
upstream        git@codeberg.org:FreeSoftwarePoland/gnome-sport.git (push)
```
aktualizacja
```
git fetch upstream #ściąga
git merge upstream/main #wlewa do wybranej gałęzi
```

## Mirrory

cdn..

*CC BY-NC-SA xmszkn*