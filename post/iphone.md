---
title: iPhone SE mount in Devuan..
---

### Instruction about mount Iphone SE in /media/Iphone as root

**First step, install apps**

```
# apt install ideviceinstaller python3-imobiledevice libimobiledevice-utils libimobiledevice6 libplist3 python3-plist ifuse usbmuxd libusbmuxd-tools
```

**then**

```
[TERMINAL] usbmuxd -f -v
[DEVICE] connect to TERMINAL via USB cable
[TERMINAL] open new shell window (may not be necessary)
[TERMINAL] idevicepair pair
[DEVICE] Trust This Computer? Tap Trust
[TERMINAL] idevicepair pair
[TERMINAL] mkdir /media/iPhone
[TERMINAL] ifuse /media/iPhone
```

@xmszkn