---
title: Technical solutions
---

### Own or friends'

[Bardzo prosty poradnik GIT](./post/poradnik_git.html) 🇵🇱

[Automatyka hdparm w Void Linux](./post/hdparm.html) 🇵🇱

[Arch Linux installation guide by @karcio](./post/archinstallation.html)

[Iphone mount in linux](./post/iphone.html)

<br><br>

### External links

[Cron examples](https://crontab.guru/examples.html)

[Jak używać SSHFS](https://www.digitalocean.com/community/tutorials/how-to-use-sshfs-to-mount-remote-file-systems-over-ssh)

[Jak pisać skrypty bash](https://rk.edu.pl/pl/bash-skrypty/)
