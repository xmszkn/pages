---
title: About
---
Hi.

I am an enthusiast of: gnu/linux, self hosting, free software, psychology, cycling and more.\
INFJ-t personality

You can chat with me:

* matrix **@xmszkn:matrix.org**

* jabber **xmszkn-at-disroot.org**